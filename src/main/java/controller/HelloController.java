package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	@RequestMapping("/home")
	public String home() {
		return "home";
	}

	@RequestMapping("/home2")
	public String home2() {
		return "home2";
	}

	@RequestMapping("/hello2")
	public String hello2() {
		return "/abc.jsp";
	}
}
